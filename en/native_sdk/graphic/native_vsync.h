/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_VSYNC_H_
#define NDK_INCLUDE_NATIVE_VSYNC_H_

/**
 * @addtogroup NativeVsync
 * @{
 *
 * @brief Provides the capabilities of native virtual synchronization (VSync).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @since 9
 * @version 1.0
 */

/**
 * @file native_vsync.h
 *
 * @brief Declares the functions for obtaining and using <b>NativeVSync</b>.
 *
 * @library libnative_vsync.so
 * @since 9
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Provides the declaration of an <b>OH_NativeVSync</b> struct.
 * @since 9
 */
struct OH_NativeVSync;

/**
 * @brief Provides the declaration of an <b>OH_NativeVSync</b> struct.
 * @since 9
 */
typedef struct OH_NativeVSync OH_NativeVSync;

/**
 * @brief Defines the pointer to a VSync callback function.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param timestamp VSync timestamp.
 * @param data Custom data.
 * @since 9
 * @version 1.0
 */
typedef void (*OH_NativeVSync_FrameCallback)(long long timestamp, void *data);

/**
 * @brief Creates an <b>OH_NativeVSync</b> instance.
 * A new <b>OH_NativeVSync</b> instance is created each time this function is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param name Pointer to the name that associates with an <b>OH_NativeVSync</b> instance.
 * @param length Length of the name.
 * @return Returns the pointer to an <b>OH_NativeVSync</b> instance.
 * @since 9
 * @version 1.0
 */
OH_NativeVSync* OH_NativeVSync_Create(const char* name, unsigned int length);

/**
 * @brief Destroys an <b>OH_NativeVSync</b> instance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync Pointer to an <b>OH_NativeVSync</b> instance.
 * @since 9
 * @version 1.0
 */
void OH_NativeVSync_Destroy(OH_NativeVSync* nativeVsync);

/**
 * @brief Requests the next VSync signal. When the signal arrives, a callback function is invoked.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync Pointer to an <b>OH_NativeVSync</b> instance.
 * @param callback Function pointer of the <b>OH_NativeVSync_FrameCallback</b> type.
 * This function pointer will be called when the next VSync signal arrives.
 * @param data Pointer to the custom data struct. The type is void*.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 9
 * @version 1.0
 */
int OH_NativeVSync_RequestFrame(OH_NativeVSync* nativeVsync, OH_NativeVSync_FrameCallback callback, void* data);

/**
 * @brief Obtains the VSync period.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeVsync
 * @param nativeVsync Pointer to an <b>OH_NativeVSync</b> instance.
 * @param period Pointer to the VSync period.
 * @return Returns <b>0</b> if the operation is successful.
 * @since 10
 * @version 1.0
 */
int OH_NativeVSync_GetPeriod(OH_NativeVSync* nativeVsync, long long* period);
#ifdef __cplusplus
}
#endif

#endif
