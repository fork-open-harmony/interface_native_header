/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file PinAuthTypes.idl
 *
 * @brief 定义口令认证驱动的枚举类和数据结构，包括认证类型，执行器角色，执行器安全等级命令ID，返回码，执行器信息，模板信息。
 *
 * 模块包路径：ohos.hdi.pin_auth.v2_0
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.pin_auth.v2_0;

/**
 * @brief 枚举用于认证的凭据类型。
 *
 * @since 3.2
 * @version 1.0
 */
enum AuthType : int {
    /** 标识认证类型是口令。 */
    PIN = 1,
    /** 标识认证类型是人脸。 */
    FACE = 2,
    /** 标识认证类型是指纹。 */
    FINGERPRINT = 4,
};

/**
 * @brief 枚举执行器角色。
 *
 * @since 3.2
 * @version 2.0
 */
enum ExecutorRole : int {
    /** 执行器角色为调度器。 */
    SCHEDULER = 0,
    /** 执行器角色为采集器，提供用户认证时的数据采集能力，需要和认证器配合完成用户认证。 */
    COLLECTOR = 1,
    /** 执行器角色为认证器，提供用户认证时数据处理能力，读取存储凭据模板信息并完成比对。 */
    VERIFIER = 2,
    /** 执行器角色为全功能执行器，可提供用户认证数据采集、处理、储存及比对能力。 */
    ALL_IN_ONE = 3,
};

/**
 * @brief 枚举执行器安全等级。
 *
 * @since 3.2
 * @version 1.0
 */
enum ExecutorSecureLevel : int {
    /** 执行器安全等级是ESL0。 */
    ESL0 = 0,
    /** 执行器安全等级是ESL1。 */
    ESL1 = 1,
    /** 执行器安全等级是ESL2。 */
    ESL2 = 2,
    /** 执行器安全等级是ESL3。 */
    ESL3 = 3,
};

/**
 * @brief 执行器信息。
 *
 * @since 3.2
 * @version 2.0
 */
struct ExecutorInfo {
    /** 传感器ID，不同传感器在口令认证驱动内的唯一标识。 */
    unsigned short sensorId;
    /** 执行器类型。 */
    unsigned int executorMatcher;
    /** 执行器角色@{ExecutorRole}。 */
    int executorRole;
    /** 用户认证类型@{AuthType}。 */
    int authType;
    /** 执行器安全等级@{ExecutorSecureLevel}。 */
    int esl;
    /** 执行器公钥，用于校验该执行器私钥签名的信息。 */
    unsigned char[] publicKey;
    /** 其他相关信息，用于支持信息扩展。 */
    unsigned char[] extraInfo;
    /** 模板的最大算法等级。 */
    unsigned int maxTemplateAcl;
};

/**
 * @brief 获取执行器属性信息。
 *
 * @since 4.0
 * @version 2.0
 */
enum GetPropertyType : int {
    /** 获取执行器的认证子类型。 */
    AUTH_SUB_TYPE = 1,
    /** 获取执行器的剩余锁定时间。 */
    LOCKOUT_DURATION = 2,
    /** 获取执行器的剩余可重试次数。 */
    REMAIN_ATTEMPTS = 3,
    /** 获取执行器的下一次失败锁定时间。 */
    NEXT_FAIL_LOCKOUT_DURATION = 6
};

/**
 * @brief 执行器属性。
 *
 * @since 4.0
 * @version 2.0
 */
struct Property {
    /** 认证子类型{@link PinSubType}。 */
    unsigned long authSubType;
    /** 剩余锁定时间。 */
    int lockoutDuration;
    /** 剩余可重试次数。 */
    int remainAttempts;
    /** 下一次失败锁定时间。 */
    int nextFailLockoutDuration;
};
/** @} */